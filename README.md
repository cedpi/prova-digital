﻿# Projeto de aplicação em Java

## Informações

##### Nome do projeto
> Prova Digital

##### Professor
> Thiago 

##### Matéria
> Técnicas em Programação I

##### Módulo
> II

##### Turma
> Técnicos em Rede e Computadores

##### Linguagem
> Java

##### IDE (_Integrated Development Environment_)
> NetBeans 1.8

> IntelliJ IDEA Community Edition

## Equipe

Desenvolvimento|Analistas|Gestores
--------|--------|-------
Bruno Sales|Ângela Pereira|Igor Pereira
Emy-lee Sirilo|Adewilson Carlos|Richardson Santos
Erick Aráujo|Anna Cascya
Guilherme França|Elizia da Costa
Gustavo Marques|Jorge Luiz
Pedro Henrique|Rodrigo Nascimento
## Objetivo

- Auxíliar e dar facilidade ao professor na aplicação da prova.