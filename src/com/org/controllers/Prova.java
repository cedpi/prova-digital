package com.org.controllers;

public class Prova
{
    private String[] questoes;
    private String[][] alternativas;
    private String[] gabarito;

    public Prova()
    {
        // Instância das String
        this.questoes = new String[3];
        this.alternativas = new String[2][4];
        this.gabarito = new String[3];
        
        this.questoes[0] = "Quanto é 2 + 2?";
        this.questoes[1] = "Quais as cores da bandeira do Brasil?";
        this.questoes[2] = "Qual o nome do Presidente do Brasil?";
        
        this.alternativas[0][0] = "3";
        this.alternativas[0][1] = "4";
        this.alternativas[0][2] = "5";
        this.alternativas[0][3] = "6";
        
        this.alternativas[1][0] = "Azul";
        this.alternativas[1][1] = "Branco";
        this.alternativas[1][2] = "Amarelo";
        this.alternativas[1][3] = "Verde";
        
        this.gabarito[0] = "4";
        this.gabarito[1] = "Azul - Branco - Amarelo - Verde";
        this.gabarito[2] = "Temer";
    }

    public String[] getQuestoes()
    {
        return questoes;
    }

    public String[][] getAlternativas()
    {
        return alternativas;
    }
    
    public String[] getGabarito()
    {
        return gabarito;
    }
    
    public void setQuestoes(String[] questoes)
    {
        this.questoes = questoes;
    }

    public void setAlternativas(String[][] alternativas)
    {
        this.alternativas = alternativas;
    }
    
    public void setGabarito(String[] gabarito)
    {
        this.gabarito = gabarito;
    }
}