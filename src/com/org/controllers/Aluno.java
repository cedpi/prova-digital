package com.org.controllers;

public class Aluno
{
    private String nome;
    private String[] respostas;

    public Aluno(String nome)
    {
        this.nome = nome;
        this.respostas = new String[3];
    }
    
    public String getNome()
    {
        return this.nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String[] getRespostas()
    {
        return this.respostas;
    }

    public void setRespostas(String[] respostas)
    {
        this.respostas = respostas;
    }
}