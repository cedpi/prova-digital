package com.org.view;

import com.org.controllers.Prova;
import java.awt.Color;

public class FormularioGabarito extends javax.swing.JFrame {
    public FormularioGabarito(Prova prova) {
        initComponents();
        
        this.getContentPane().setBackground(Color.white);

        this.txtRespostaCorreta01.setText(prova.getGabarito()[0]);
        this.txtRespostaCorreta02.setText(prova.getGabarito()[1]);
        this.txtRespostaCorreta03.setText(prova.getGabarito()[2]);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblP = new javax.swing.JLabel();
        lblQuestao5 = new javax.swing.JLabel();
        lblQuestao6 = new javax.swing.JLabel();
        txtRespostaAluno01 = new javax.swing.JTextField();
        lblQuestao7 = new javax.swing.JLabel();
        txtRespostaCorreta01 = new javax.swing.JTextField();
        lblQuestao8 = new javax.swing.JLabel();
        lblQuestao9 = new javax.swing.JLabel();
        txtRespostaAluno02 = new javax.swing.JTextField();
        lblQuestao10 = new javax.swing.JLabel();
        txtRespostaCorreta02 = new javax.swing.JTextField();
        lblQuestao11 = new javax.swing.JLabel();
        lblQuestao12 = new javax.swing.JLabel();
        lblQuestao13 = new javax.swing.JLabel();
        txtRespostaAluno03 = new javax.swing.JTextField();
        txtRespostaCorreta03 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblProfessor = new javax.swing.JLabel();
        lblAluno = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(421, 430));
        setPreferredSize(new java.awt.Dimension(421, 430));
        setResizable(false);

        lblP.setText("Professor:");

        lblQuestao5.setText("Resposta aluno:");

        lblQuestao6.setText("Questão 01");

        txtRespostaAluno01.setEditable(false);
        txtRespostaAluno01.setText("Resposta 1");
        txtRespostaAluno01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaAluno01ActionPerformed(evt);
            }
        });

        lblQuestao7.setText("Resposta Correta:");

        txtRespostaCorreta01.setEditable(false);
        txtRespostaCorreta01.setText("Resposta 1");
        txtRespostaCorreta01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaCorreta01ActionPerformed(evt);
            }
        });

        lblQuestao8.setText("Questão 02");

        lblQuestao9.setText("Resposta aluno:");

        txtRespostaAluno02.setEditable(false);
        txtRespostaAluno02.setText("Resposta 1");
        txtRespostaAluno02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaAluno02ActionPerformed(evt);
            }
        });

        lblQuestao10.setText("Resposta Correta:");

        txtRespostaCorreta02.setEditable(false);
        txtRespostaCorreta02.setText("Resposta 1");
        txtRespostaCorreta02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaCorreta02ActionPerformed(evt);
            }
        });

        lblQuestao11.setText("Questão 03");

        lblQuestao12.setText("Resposta aluno:");

        lblQuestao13.setText("Resposta Correta:");

        txtRespostaAluno03.setEditable(false);
        txtRespostaAluno03.setText("Resposta 1");
        txtRespostaAluno03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaAluno03ActionPerformed(evt);
            }
        });

        txtRespostaCorreta03.setEditable(false);
        txtRespostaCorreta03.setText("Resposta 1");
        txtRespostaCorreta03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaCorreta03ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/org/img/Seduce.jpeg"))); // NOI18N

        jLabel2.setText("Aluno:");

        lblProfessor.setText("jLabel3");

        lblAluno.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblQuestao6)
                    .addComponent(lblQuestao8)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblQuestao5)
                            .addComponent(lblQuestao7)
                            .addComponent(lblQuestao9)
                            .addComponent(lblQuestao10)
                            .addComponent(lblQuestao11)
                            .addComponent(lblQuestao12)
                            .addComponent(lblQuestao13))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtRespostaAluno01)
                            .addComponent(txtRespostaCorreta01)
                            .addComponent(txtRespostaCorreta02)
                            .addComponent(txtRespostaAluno03)
                            .addComponent(txtRespostaCorreta03)
                            .addComponent(txtRespostaAluno02, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblP)
                                .addGap(18, 18, 18)
                                .addComponent(lblProfessor))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblAluno)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblP, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblProfessor))
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(lblAluno))
                        .addGap(33, 33, 33))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(lblQuestao6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao5)
                    .addComponent(txtRespostaAluno01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao7)
                    .addComponent(txtRespostaCorreta01, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(lblQuestao8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao9)
                    .addComponent(txtRespostaAluno02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao10)
                    .addComponent(txtRespostaCorreta02, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblQuestao11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao12)
                    .addComponent(txtRespostaAluno03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestao13)
                    .addComponent(txtRespostaCorreta03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(421, 393));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtRespostaAluno01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaAluno01ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaAluno01ActionPerformed

    private void txtRespostaCorreta01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaCorreta01ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaCorreta01ActionPerformed

    private void txtRespostaAluno02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaAluno02ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaAluno02ActionPerformed

    private void txtRespostaCorreta02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaCorreta02ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaCorreta02ActionPerformed

    private void txtRespostaAluno03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaAluno03ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaAluno03ActionPerformed

    private void txtRespostaCorreta03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaCorreta03ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaCorreta03ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    public javax.swing.JLabel lblAluno;
    private javax.swing.JLabel lblP;
    public javax.swing.JLabel lblProfessor;
    public javax.swing.JLabel lblQuestao10;
    public javax.swing.JLabel lblQuestao11;
    public javax.swing.JLabel lblQuestao12;
    public javax.swing.JLabel lblQuestao13;
    public javax.swing.JLabel lblQuestao5;
    public javax.swing.JLabel lblQuestao6;
    public javax.swing.JLabel lblQuestao7;
    public javax.swing.JLabel lblQuestao8;
    public javax.swing.JLabel lblQuestao9;
    public javax.swing.JTextField txtRespostaAluno01;
    public javax.swing.JTextField txtRespostaAluno02;
    public javax.swing.JTextField txtRespostaAluno03;
    public javax.swing.JTextField txtRespostaCorreta01;
    public javax.swing.JTextField txtRespostaCorreta02;
    public javax.swing.JTextField txtRespostaCorreta03;
    // End of variables declaration//GEN-END:variables
}
