package com.org.view;

import com.org.controllers.Prova;
import java.awt.Color;
import javax.swing.JOptionPane;

public class FormularioProfessor extends javax.swing.JFrame
{
    private Prova prova = new Prova();
    
    public FormularioProfessor()
    {
        initComponents();

        this.getContentPane().setBackground(Color.white);
        
        do {
            lblProfessor.setText(JOptionPane.showInputDialog("Informe o nome do professor"));
        } while (lblProfessor.getText().equals(""));
        do {
            lblDisciplina.setText(JOptionPane.showInputDialog("Informe o nome da disciplina"));
        } while (lblDisciplina.getText().equals(""));
        do {
            lblSerie.setText(JOptionPane.showInputDialog("Informe a série"));
        } while (lblSerie.getText().equals(""));
        
        jrbGroup.add(jrbAlt1);
        jrbGroup.add(jrbalt2);
        jrbGroup.add(jrbalt3);
        jrbGroup.add(jrbalt4);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jRadioButton5 = new javax.swing.JRadioButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jrbGroup = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblProfessor = new javax.swing.JLabel();
        lblDisciplina = new javax.swing.JLabel();
        lblSerie = new javax.swing.JLabel();
        lblQuestao01 = new javax.swing.JLabel();
        jcbAlt4 = new javax.swing.JCheckBox();
        jcbAlt3 = new javax.swing.JCheckBox();
        jcbAlt2 = new javax.swing.JCheckBox();
        jcbAlt1 = new javax.swing.JCheckBox();
        lblQuestao2 = new javax.swing.JLabel();
        jrbAlt1 = new javax.swing.JRadioButton();
        jrbalt2 = new javax.swing.JRadioButton();
        jrbalt3 = new javax.swing.JRadioButton();
        jrbalt4 = new javax.swing.JRadioButton();
        lblQuestao3 = new javax.swing.JLabel();
        txtAlt1Quest1 = new javax.swing.JTextField();
        txtAlt4Quest1 = new javax.swing.JTextField();
        txtAlt3Quest1 = new javax.swing.JTextField();
        txtAlt2Quest1 = new javax.swing.JTextField();
        txtAlt1Quest2 = new javax.swing.JTextField();
        txtAlt2Quest2 = new javax.swing.JTextField();
        txtAlt3Quest2 = new javax.swing.JTextField();
        txtAlt4Quest2 = new javax.swing.JTextField();
        btnConfirmar = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtRespostaCorreta03 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtQuestao02 = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtQuestao03 = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtQuestao01 = new javax.swing.JTextArea();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jRadioButton5.setText("jRadioButton5");

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(602, 900));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 24)); // NOI18N
        jLabel1.setText("COLÉGIO ESTADUAL DOM PEDRO I");
        jLabel1.setToolTipText("");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/org/img/WhatsApp Image 2018-10-22 at 17.06.15.jpeg"))); // NOI18N

        jLabel4.setText("Nome Professor:");

        jLabel5.setText("Disciplina:");

        jLabel6.setText("Série:");

        lblProfessor.setText("Professor");

        lblDisciplina.setText("Disciplina");

        lblSerie.setText("Serie");

        lblQuestao01.setText("Questão 01");

        jcbAlt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbAlt1ActionPerformed(evt);
            }
        });

        lblQuestao2.setText("Questão 02");

        jrbAlt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbAlt1ActionPerformed(evt);
            }
        });

        lblQuestao3.setText("Questão 03");

        txtAlt1Quest2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAlt1Quest2ActionPerformed(evt);
            }
        });

        txtAlt4Quest2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAlt4Quest2ActionPerformed(evt);
            }
        });

        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        btnGravar.setText("Gravar");
        btnGravar.setEnabled(false);
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });

        jLabel7.setText("Resposta esperada");

        txtRespostaCorreta03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRespostaCorreta03ActionPerformed(evt);
            }
        });

        txtQuestao02.setColumns(20);
        txtQuestao02.setLineWrap(true);
        txtQuestao02.setRows(1);
        jScrollPane2.setViewportView(txtQuestao02);

        txtQuestao03.setColumns(20);
        txtQuestao03.setLineWrap(true);
        txtQuestao03.setRows(1);
        jScrollPane3.setViewportView(txtQuestao03);

        txtQuestao01.setColumns(20);
        txtQuestao01.setRows(1);
        jScrollPane4.setViewportView(txtQuestao01);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuestao3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jcbAlt2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtAlt2Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jcbAlt4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt4Quest2))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jcbAlt3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt3Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 531, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtRespostaCorreta03, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addGap(25, 25, 25)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblSerie)
                                            .addComponent(lblDisciplina)
                                            .addComponent(lblProfessor)))
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblQuestao01)
                            .addComponent(lblQuestao2)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jrbalt4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt4Quest1))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jrbalt3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt3Quest1))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jrbalt2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt2Quest1))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jrbAlt1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtAlt1Quest1, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jcbAlt1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtAlt1Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addComponent(btnConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(lblProfessor))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(lblDisciplina))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(lblSerie))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblQuestao01)
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addComponent(jLabel3))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel2)))
                        .addGap(31, 31, 31)))
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jrbAlt1)
                    .addComponent(txtAlt1Quest1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jrbalt2)
                    .addComponent(txtAlt2Quest1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jrbalt3)
                    .addComponent(txtAlt3Quest1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jrbalt4)
                    .addComponent(txtAlt4Quest1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblQuestao2)
                .addGap(16, 16, 16)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jcbAlt1)
                            .addComponent(txtAlt1Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcbAlt2))
                    .addComponent(txtAlt2Quest2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jcbAlt3)
                    .addComponent(txtAlt3Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jcbAlt4)
                    .addComponent(txtAlt4Quest2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblQuestao3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtRespostaCorreta03, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirmar)
                    .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.getAccessibleContext().setAccessibleName("");
        btnGravar.getAccessibleContext().setAccessibleName("");

        setSize(new java.awt.Dimension(602, 773));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jcbAlt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbAlt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbAlt1ActionPerformed

    private void jrbAlt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbAlt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrbAlt1ActionPerformed

    private void txtAlt4Quest2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAlt4Quest2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAlt4Quest2ActionPerformed

    private void txtAlt1Quest2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAlt1Quest2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAlt1Quest2ActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        String[] questoes = new String[3];
        questoes[0] = "Questão 01: " + txtQuestao01.getText();
        questoes[1] = "Questão 02: " + txtQuestao02.getText();
        questoes[2] = "Questão 03: " + txtQuestao03.getText();
        prova.setQuestoes(questoes);
        
        String[][] alternativas = new String[2][4];
        alternativas[0][0] = txtAlt1Quest1.getText();
        alternativas[0][1] = txtAlt2Quest1.getText();
        alternativas[0][2] = txtAlt3Quest1.getText();
        alternativas[0][3] = txtAlt4Quest1.getText();
                
        alternativas[1][0] = txtAlt1Quest2.getText();
        alternativas[1][1] = txtAlt2Quest2.getText();
        alternativas[1][2] = txtAlt3Quest2.getText();
        alternativas[1][3] = txtAlt4Quest2.getText();
        
        prova.setAlternativas(alternativas);
        
        String[] gabarito = new String[3];
        if (jrbAlt1.isSelected()) {
            gabarito[0] = txtAlt1Quest1.getText();
        } else if (jrbalt2.isSelected()) {
            gabarito[0] = txtAlt2Quest1.getText();
        } else if (jrbalt3.isSelected()) {
            gabarito[0] = txtAlt3Quest1.getText();
        } else if (jrbalt4.isSelected()) {
            gabarito[0] = txtAlt4Quest1.getText();
        } else {
            JOptionPane.showMessageDialog(null, "Marque a resposta correta da alternativa!", "Prova", JOptionPane.ERROR_MESSAGE);
        }
        
        String resp1 = "";
        if (jcbAlt1.isSelected())
        {
            resp1 += txtAlt1Quest2.getText();
        }

        if (jcbAlt2.isSelected())
        {
            resp1 += " - " + txtAlt2Quest2.getText();
        }

        if (jcbAlt3.isSelected())
        {
            resp1 += " - " + txtAlt2Quest2.getText();
        }

        if (jcbAlt4.isSelected())
        {
            resp1 += " - " + txtAlt2Quest2.getText();
        }

        if (!jcbAlt1.isSelected() && !jcbAlt2.isSelected() && !jcbAlt3.isSelected() && !jcbAlt4.isSelected())
        {
            JOptionPane.showMessageDialog(null, "Marque a resposta correta da alternativa!", "Prova", JOptionPane.ERROR_MESSAGE);
        }

        gabarito[1] = resp1;
        
        gabarito[2] = txtRespostaCorreta03.getText();
        
        prova.setGabarito(gabarito);
        
        this.setVisible(false);
        
        FormularioAluno fAluno = new FormularioAluno(prova, false);
        fAluno.lblProfessor.setText(this.lblProfessor.getText());
        fAluno.lblDisciplina.setText(this.lblDisciplina.getText());
        fAluno.lblSerie.setText(this.lblSerie.getText());
        fAluno.setVisible(true);
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        btnGravar.setEnabled(true);
        JOptionPane.showMessageDialog(null, "Verifique todas as perguntas antes de enviar!", "Prova", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void txtRespostaCorreta03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRespostaCorreta03ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRespostaCorreta03ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JButton btnGravar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    public javax.swing.JCheckBox jcbAlt1;
    public javax.swing.JCheckBox jcbAlt2;
    public javax.swing.JCheckBox jcbAlt3;
    public javax.swing.JCheckBox jcbAlt4;
    public javax.swing.JRadioButton jrbAlt1;
    private javax.swing.ButtonGroup jrbGroup;
    public javax.swing.JRadioButton jrbalt2;
    public javax.swing.JRadioButton jrbalt3;
    public javax.swing.JRadioButton jrbalt4;
    public javax.swing.JLabel lblDisciplina;
    public javax.swing.JLabel lblProfessor;
    public javax.swing.JLabel lblQuestao01;
    public javax.swing.JLabel lblQuestao2;
    public javax.swing.JLabel lblQuestao3;
    public javax.swing.JLabel lblSerie;
    public javax.swing.JTextField txtAlt1Quest1;
    private javax.swing.JTextField txtAlt1Quest2;
    public javax.swing.JTextField txtAlt2Quest1;
    private javax.swing.JTextField txtAlt2Quest2;
    private javax.swing.JTextField txtAlt3Quest1;
    private javax.swing.JTextField txtAlt3Quest2;
    private javax.swing.JTextField txtAlt4Quest1;
    private javax.swing.JTextField txtAlt4Quest2;
    private javax.swing.JTextArea txtQuestao01;
    private javax.swing.JTextArea txtQuestao02;
    private javax.swing.JTextArea txtQuestao03;
    private javax.swing.JTextField txtRespostaCorreta03;
    // End of variables declaration//GEN-END:variables
}
