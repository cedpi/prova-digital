package com.org.view;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import com.org.controllers.Aluno;
import com.org.controllers.Prova;
import java.awt.Color;

public class FormularioAluno extends javax.swing.JFrame
{
    private String[] respostas = new String[3];
    private FormularioGabarito gab;
    
    public FormularioAluno(Prova prova, boolean autoShow)
    {
        initComponents();

        gab = new FormularioGabarito(prova);

        this.getContentPane().setBackground(Color.white);        
        
        jrbAlt1.setText(prova.getAlternativas()[0][0]);
        jrbAlt2.setText(prova.getAlternativas()[0][1]);
        jrbAlt3.setText(prova.getAlternativas()[0][2]);
        jrbAlt4.setText(prova.getAlternativas()[0][3]);
        
        jrbGroup.add(jrbAlt1);
        jrbGroup.add(jrbAlt2);
        jrbGroup.add(jrbAlt3);
        jrbGroup.add(jrbAlt4);
        
        lblQuestao01.setText(prova.getQuestoes()[0]);
        lblQuestao02.setText(prova.getQuestoes()[1]);
        lblQuestao03.setText(prova.getQuestoes()[2]);
        
        jcbAlt1.setText(prova.getAlternativas()[1][0]);
        jcbAlt2.setText(prova.getAlternativas()[1][1]);
        jcbAlt3.setText(prova.getAlternativas()[1][2]);
        jcbAlt4.setText(prova.getAlternativas()[1][3]);
        
        if (autoShow)
        {
            do {
                lblProfessor.setText(JOptionPane.showInputDialog("Informe o nome do professor"));
            } while (lblProfessor.getText().equals(""));
            do {
                lblDisciplina.setText(JOptionPane.showInputDialog("Informe o nome da disciplina"));
            } while (lblDisciplina.getText().equals(""));
            do {
                lblSerie.setText(JOptionPane.showInputDialog("Informe a série"));
            } while (lblSerie.getText().equals(""));
            do {
                lblTurno.setText(JOptionPane.showInputDialog("Informe o turno"));
            } while (lblTurno.getText().equals(""));
            do {
                lblAluno.setText(JOptionPane.showInputDialog("Informe o nome do aluno"));
            } while (lblAluno.getText().equals(""));
        }
        else
        {
            do {
                lblTurno.setText(JOptionPane.showInputDialog("Informe o turno"));
            } while (lblTurno.getText().equals(""));
            do {
                lblAluno.setText(JOptionPane.showInputDialog("Informe o nome do aluno"));
            } while (lblAluno.getText().equals(""));
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jRadioButton5 = new javax.swing.JRadioButton();
        jrbGroup = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblProfessor = new javax.swing.JLabel();
        lblDisciplina = new javax.swing.JLabel();
        lblSerie = new javax.swing.JLabel();
        lblTurno = new javax.swing.JLabel();
        lblAluno = new javax.swing.JLabel();
        lblQuestao01 = new javax.swing.JLabel();
        jcbAlt4 = new javax.swing.JCheckBox();
        jcbAlt3 = new javax.swing.JCheckBox();
        jcbAlt2 = new javax.swing.JCheckBox();
        jcbAlt1 = new javax.swing.JCheckBox();
        lblQuestao02 = new javax.swing.JLabel();
        jrbAlt1 = new javax.swing.JRadioButton();
        jrbAlt2 = new javax.swing.JRadioButton();
        jrbAlt3 = new javax.swing.JRadioButton();
        jrbAlt4 = new javax.swing.JRadioButton();
        jLabel16 = new javax.swing.JLabel();
        btnPronto = new javax.swing.JButton();
        lblQuestao03 = new javax.swing.JLabel();
        txtQuestao3 = new javax.swing.JTextField();
        btnConfirmar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtRespostas = new javax.swing.JTextPane();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jRadioButton5.setText("jRadioButton5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(618, 860));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 24)); // NOI18N
        jLabel1.setText("COLÉGIO ESTADUAL DOM PEDRO I");
        jLabel1.setToolTipText("");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/org/img/WhatsApp Image 2018-10-22 at 17.06.15.jpeg"))); // NOI18N

        jLabel4.setText("Nome Professor:");

        jLabel5.setText("Disciplina:");

        jLabel6.setText("Série:");

        jLabel7.setText("Turno:");

        jLabel8.setText("Nome Aluno:");

        lblProfessor.setText("Professor");

        lblDisciplina.setText("Disciplina");

        lblSerie.setText("Serie");

        lblTurno.setText("Turno");

        lblAluno.setText("Aluno");

        lblQuestao01.setText("Questão 01");

        jcbAlt4.setText("jCheckBox2");

        jcbAlt3.setText("jCheckBox2");

        jcbAlt2.setText("jCheckBox2");

        jcbAlt1.setText("jCheckBox2");
        jcbAlt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbAlt1ActionPerformed(evt);
            }
        });

        lblQuestao02.setText("Questão 02");

        jrbAlt1.setText("jRadioButton1");

        jrbAlt2.setText("jRadioButton2");

        jrbAlt3.setText("jRadioButton3");

        jrbAlt4.setText("jRadioButton4");

        jLabel16.setText("Respostas:");

        btnPronto.setText("Pronto");
        btnPronto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProntoActionPerformed(evt);
            }
        });

        lblQuestao03.setText("Questão 03");

        btnConfirmar.setText("Confirmar Respostas e enviar?");
        btnConfirmar.setEnabled(false);
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        txtRespostas.setEditable(false);
        jScrollPane2.setViewportView(txtRespostas);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSerie)
                                    .addComponent(lblDisciplina)
                                    .addComponent(lblProfessor)
                                    .addComponent(lblTurno)
                                    .addComponent(lblAluno)))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnConfirmar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblQuestao01)
                                    .addComponent(lblQuestao02)
                                    .addComponent(jcbAlt2)
                                    .addComponent(jcbAlt3)
                                    .addComponent(jcbAlt4)
                                    .addComponent(jrbAlt3)
                                    .addComponent(jcbAlt1)
                                    .addComponent(jrbAlt4)
                                    .addComponent(jrbAlt2)
                                    .addComponent(jrbAlt1)
                                    .addComponent(txtQuestao3, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblQuestao03))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnPronto)))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(11, 11, 11)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(lblProfessor))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(lblDisciplina))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(lblSerie))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblTurno))))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAluno)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addComponent(lblQuestao01)
                .addGap(22, 22, 22)
                .addComponent(jrbAlt1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jrbAlt2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jrbAlt3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jrbAlt4)
                .addGap(24, 24, 24)
                .addComponent(lblQuestao02)
                .addGap(18, 18, 18)
                .addComponent(jcbAlt1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbAlt2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbAlt3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jcbAlt4)
                .addGap(18, 18, 18)
                .addComponent(lblQuestao03, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtQuestao3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPronto))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnConfirmar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.getAccessibleContext().setAccessibleName("");

        setSize(new java.awt.Dimension(618, 833));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jcbAlt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbAlt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbAlt1ActionPerformed

    private void btnProntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProntoActionPerformed
        Aluno aluno = new Aluno(lblAluno.getText());
        
        
        if (jrbAlt1.isSelected()) {
            respostas[0] = jrbAlt1.getText();
        } else if (jrbAlt2.isSelected()) {
            respostas[0] = jrbAlt2.getText();
        } else if (jrbAlt3.isSelected()) {
            respostas[0] = jrbAlt3.getText();
        } else if (jrbAlt4.isSelected()) {
            respostas[0] = jrbAlt4.getText();
        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma alternativa para a questão 1!", "Prova", JOptionPane.ERROR_MESSAGE);
        }
        
        String resp1 = "";
        if (jcbAlt1.isSelected())
        {
            resp1 += jcbAlt1.getText();
        }
        
        if (jcbAlt2.isSelected())
        {
            resp1 += " - " + jcbAlt2.getText();
        }
        
        if (jcbAlt3.isSelected())
        {
            resp1 += " - " + jcbAlt3.getText();
        }
        
        if (jcbAlt4.isSelected())
        {
            resp1 += " - " + jcbAlt4.getText();
        }
        
        if (!jcbAlt1.isSelected() && !jcbAlt2.isSelected() && !jcbAlt3.isSelected() && !jcbAlt4.isSelected())
        {
            JOptionPane.showMessageDialog(null, "Selecione uma alternativa para a questão 2!", "Prova", JOptionPane.ERROR_MESSAGE);
        }
        
        respostas[1] = resp1;
        
        if (!txtQuestao3.getText().isEmpty())
        {
            respostas[2] = txtQuestao3.getText();
        }
        else
        {
            JOptionPane.showMessageDialog(null, "O campo da reposta 3 deve ser preenchido!", "Prova", JOptionPane.ERROR_MESSAGE);
        }
        
        if (respostas[0] != null && respostas[1] != null && respostas[2] != null) {
            aluno.setRespostas(respostas);
            String resp = lblQuestao01.getText() + " " + respostas[0] + ".\n" + lblQuestao02.getText() + " " + respostas[1] + ".\n" + lblQuestao03.getText() + " " + respostas[2] + ".";
            txtRespostas.setText(resp);

            btnConfirmar.setEnabled(true);
        }
    }//GEN-LAST:event_btnProntoActionPerformed

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
          try
          {
            FileWriter writer = new FileWriter("Respostas Aluno " + lblAluno.getText() + " - Disciplina " + lblDisciplina.getText() + ".txt");
            PrintWriter printer = new PrintWriter(writer);
            
            printer.println("Colégio Estadual Dom Pedro I");
            printer.println("Nome do professor: " + lblProfessor.getText());
            printer.println("Nome do aluno: " + lblAluno.getText());
            printer.println("Série: " + lblSerie.getText());
            printer.println("Turno: " + lblTurno.getText());
            printer.println("");
            
            String[] questoes = txtRespostas.getText().split(".\n");
            
            for (String resp : questoes) {
                printer.println(resp);
            }
            
            printer.close();
            writer.close();
            JOptionPane.showMessageDialog(null, "Respostas enviadas com sucesso!");
            this.setVisible(false);
            
            gab.lblProfessor.setText(this.lblProfessor.getText());
            gab.lblAluno.setText(this.lblAluno.getText());
            
            gab.txtRespostaAluno01.setText(respostas[0]);
            gab.txtRespostaAluno02.setText(respostas[1]);
            gab.txtRespostaAluno03.setText(respostas[2]);
            
            gab.setVisible(true);
          }
          catch (IOException e)
          {
              JOptionPane.showMessageDialog(null, "Falha ao enviar respostas!\n" + e.getMessage(), "Prova", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_btnConfirmarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnConfirmar;
    public javax.swing.JButton btnPronto;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    public javax.swing.JCheckBox jcbAlt1;
    public javax.swing.JCheckBox jcbAlt2;
    public javax.swing.JCheckBox jcbAlt3;
    public javax.swing.JCheckBox jcbAlt4;
    public javax.swing.JRadioButton jrbAlt1;
    public javax.swing.JRadioButton jrbAlt2;
    public javax.swing.JRadioButton jrbAlt3;
    public javax.swing.JRadioButton jrbAlt4;
    private javax.swing.ButtonGroup jrbGroup;
    public javax.swing.JLabel lblAluno;
    public javax.swing.JLabel lblDisciplina;
    public javax.swing.JLabel lblProfessor;
    public javax.swing.JLabel lblQuestao01;
    public javax.swing.JLabel lblQuestao02;
    public javax.swing.JLabel lblQuestao03;
    public javax.swing.JLabel lblSerie;
    public javax.swing.JLabel lblTurno;
    public javax.swing.JTextField txtQuestao3;
    public javax.swing.JTextPane txtRespostas;
    // End of variables declaration//GEN-END:variables
}
